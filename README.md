# Return in Echoes TAS

Quick and dirty Python script to TAS the game [Return in Echoes](https://virtualharby.itch.io/return-in-echoes) by the amazing [VirtualHarby](https://www.youtube.com/@virtualharby)!

Best time I got was 15 seconds, I had to slow the TAS down due to limitations in the engine/browser.

![Demo video](return-in-echoes-tas.mp4)

Enjoy!

Made with ❤️ by [Kinami Imai 今井きなみ](https://ikinami.gitlab.io/kinami/)