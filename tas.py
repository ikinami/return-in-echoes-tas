from evdev import ecodes as e, UInput
import sys
import time

cap = {
	e.EV_KEY: [e.KEY_A, e.KEY_W, e.KEY_S, e.KEY_D, e.KEY_ENTER],
	e.EV_MSC: []
}
ui = UInput(cap)
time.sleep(2)

ui.write(e.EV_KEY, e.EV_MSC, 458756)
ui.write(e.EV_KEY, e.KEY_ENTER, 1)
ui.write(e.EV_KEY, e.KEY_ENTER, 0)
ui.syn()

time.sleep(0.05)

for line in sys.stdin:
	amount = int(line[1:])
	key_code = 0
	if line[0] == 'A':
		key_code = e.KEY_A
	if line[0] == 'W':
		key_code = e.KEY_W
	if line[0] == 'S':
		key_code = e.KEY_S
	if line[0] == 'D':
		key_code = e.KEY_D
	if line[0] == 'E':
		key_code = e.KEY_ENTER
	if line[0] == 'T':
		time.sleep(amount*0.95)
		continue
	if key_code == 0:
		print('Something wrong with input')
		exit(1)
	for i in range(amount):
		time.sleep(0.02)
		ui.write(e.EV_KEY, e.EV_MSC, 458756)
		ui.write(e.EV_KEY, key_code, 1)
		ui.write(e.EV_KEY, e.EV_MSC, 458756)
		ui.write(e.EV_KEY, key_code, 0)
		ui.syn()
	time.sleep(0.04)
ui.close()

exit()
